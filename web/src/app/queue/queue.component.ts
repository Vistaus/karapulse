import {Component, OnDestroy, OnInit} from '@angular/core';
import {QueueEntry, Status} from '../status';
import {SongDetailsComponent} from '../song-details/song-details.component';
import {MatBottomSheet} from '@angular/material';
import {KaraokeService} from '../karaoke.service';
import {interval, Observable, Subscription, merge, Subject} from 'rxjs';
import {startWith, switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-queue',
  templateUrl: './queue.component.html',
  styleUrls: ['./queue.component.css']
})
export class QueueComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['user', 'artist', 'title'];
  currentQueue: QueueEntry[];
  poller: Subscription;
  refreshTrigger: Subject<void>;

  constructor(private bottomSheet: MatBottomSheet, private karaokeService: KaraokeService) { }

  ngOnInit() {
    this.karaokeService.statusEvent$.subscribe(status => this.currentQueue = status.queue);
  }

  ngOnDestroy(): void {
    if (this.poller) {
      this.poller.unsubscribe();
    }
  }

  refreshQueue(): void {
    if (this.refreshTrigger) {
      this.refreshTrigger.next();
    }
  }
  // openBottomSheet(row): void {
  //   console.log(row);
  //   this.bottomSheet.open(SongDetailsComponent, {data: row});
  // }
}
