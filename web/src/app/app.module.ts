import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {HttpClientModule} from '@angular/common/http';
import {PlayerControlComponent} from './playercontrol/playercontrol.component';
import {SearchResultsComponent} from './searchresults/searchresults.component';
import {SearchBarComponent} from './searchbar/searchbar.component';
import {ProfileComponent} from './profile/profile.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {
  MatIconModule,
  MatMenuModule,
  MatButtonModule,
  MatInputModule,
  MatListModule,
  MatDialogModule,
  MatTabsModule
} from '@angular/material';
import {MatTableModule} from '@angular/material/table';
import {FormsModule} from '@angular/forms';
import {SearchComponent} from './search/search.component';
import {SongDetailsComponent} from './song-details/song-details.component';
import {MatBottomSheetModule} from '@angular/material';
import {FirstRunDialogComponent, NameDialogComponent} from './first-run-dialog/first-run-dialog.component';

import {HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import {InMemoryDataService} from './in-memory-data.service';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import { QueueComponent } from './queue/queue.component';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';

@NgModule({
  declarations: [
    AppComponent,
    PlayerControlComponent,
    SearchResultsComponent,
    SearchBarComponent,
    ProfileComponent,
    DashboardComponent,
    SearchComponent,
    SongDetailsComponent,
    FirstRunDialogComponent,
    NameDialogComponent,
    QueueComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    FormsModule,
    MatTableModule,
    MatListModule,
    MatBottomSheetModule,
    MatDialogModule,
    MatTabsModule,
    /*HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, {dataEncapsulation: false}),*/
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}, {
    provide: HAMMER_GESTURE_CONFIG, useClass: HammerGestureConfig
  }],
  bootstrap: [AppComponent],
  entryComponents: [SongDetailsComponent, NameDialogComponent],
})
export class AppModule {
}
