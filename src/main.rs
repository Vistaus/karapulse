// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate log;
#[macro_use]
extern crate failure;

use env_logger::{Builder, Env};
use failure::Error;

use chrono::Duration;
use gettextrs::*;
use gio::prelude::*;
use gstreamer as gst;
use gtk::prelude::*;
use std::cell::RefCell;
use std::path::{Path, PathBuf};
use std::thread;
use structopt::StructOpt;

mod common;
mod config;
mod db;
mod karapulse;
mod player;
mod protocol;
mod queue;
mod web;

#[cfg(test)]
mod tests;

use crate::common::init;
use crate::db::DB;
use crate::karapulse::Karapulse;
use crate::web::start_web;
use config::{GETTEXT_PACKAGE, LOCALEDIR};

#[derive(StructOpt, Debug)]
#[structopt(name = "karapulse")]
struct Opt {
    #[structopt(short = "p", long = "play", parse(from_os_str), help = "Files to play")]
    medias: Vec<PathBuf>,
    #[structopt(name = "db")]
    path: Option<PathBuf>,
    #[structopt(short = "r", long = "restore", help = "Restore queue")]
    restore: bool,
}

fn restore_queue(karapulse: &Karapulse, db: &DB) {
    let history = db.history_not_played().unwrap();
    let last = match history.last() {
        Some(h) => h.queued,
        None => return,
    };
    info!("restoring queue from {}", last);

    for h in history {
        if last - h.queued > Duration::days(1) {
            trace!("too old, skip {:?}", h);
        } else {
            debug!("restore {:?}", h);
            let path = Path::new(&h.song.path).to_path_buf();
            karapulse
                .enqueue(&path, &h.user, Some(&h.song), Some(h.rowid))
                .unwrap();
        }
    }
}

fn main() -> Result<(), Error> {
    Builder::from_env(Env::default().default_filter_or("warn")).init();
    init()?;

    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
    textdomain(GETTEXT_PACKAGE);

    let opt = Opt::from_args();

    let application = gtk::Application::new(Some(config::APP_ID), Default::default())?;

    glib::set_application_name(&format!("Karapulse{}", config::NAME_SUFFIX));

    application.connect_startup(move |app| {
        let (tx, rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        let db = DB::new(&opt.path).unwrap();

        let window = gtk::ApplicationWindow::new(app);

        app.inhibit(
            Some(&window),
            gtk::ApplicationInhibitFlags::IDLE,
            Some("Karapulse running"),
        );

        let window = window.upcast::<gtk::Window>();
        let player = player::Player::new(window, tx.clone()).unwrap();
        let karapulse = Karapulse::new(player, rx, db);

        let db = DB::new(&opt.path).unwrap();
        if opt.restore {
            restore_queue(&karapulse, &db);
        }

        for media in opt.medias.clone() {
            info!("queing {}", media.display());
            /* FIXME: fetch info from file */
            karapulse.enqueue(&media, "CLI", None, None).unwrap();
        }

        karapulse.play().unwrap();

        /* Start web server in its own thread*/
        thread::spawn(move || {
            start_web(tx, db);
        });

        app.connect_activate(move |_app| {
            // TODO: present window
        });

        // Pass ownership of karapulse to the cell and the cell
        // to the callback so karapulse stays alive until shutdown
        let container = RefCell::new(Some(karapulse));
        app.connect_shutdown(move |_| {
            let _karapulse = container
                .borrow_mut()
                .take()
                .expect("Shutdown called multiple times");
        });
    });

    application.run(&[]);

    debug!("exiting");
    unsafe {
        gst::deinit();
    }
    Ok(())
}
